import pygame
#definição de cores em RGB
WHITE = (255,255,255)
BLACK = (0,0,0)
DARKGRAY = (40,40,40)
LIGHTGREY = (100,100,100)
GREEN = (0,255,0)
RED = (255,0,0)
YELLOW = (255,255,0)
BROWN = (106,55,5)

#configs do jogo
WIDTH = 800 #1024
HEIGHT = 600 #768
FPS = 60
TITLE = "Nome do jogo"
BGCOLOR = BROWN

TILESIZE = 64
GRIDWIDTH = WIDTH/TILESIZE
GRIDHEIGHT = HEIGHT/TILESIZE

WALL_IMG = 'parede_verde.png'

# configurações do player
PLAYER_SPEED = 250
PLAYER_ROT_SPEED = 250
PLAYER_IMG = 'manBlue_gun.png'
PLAYER_HIT_RECT = pygame.Rect(0,0,35,35)


#configuração do inimigo
MOB_IMG = 'mob1.png'
MOB_SPEED = 150
MOB_HIT_RECT = pygame.Rect(0,0,30,30)
